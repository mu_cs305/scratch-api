Junior Master Gardner App
=========================

Overview
--------
This app is designed to assess and track the progress of low-literacy students enrolled in the Junior Master Gardner program
through simple web-based games.  The web app is built with Ruby on Rails.

Contact
-------
sethmbaker@gmail.com
joe.rawlinson@gmail.com
zachydj@gmail.com