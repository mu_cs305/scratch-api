namespace JMG.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;


    // TODO Determine whether this should handle binary files as well
    // TODO Figure out why fields in the child classes cause migration errors
    public abstract class Image
    {
        public Image()
        {

        }
        [Key]
        public int imageID { get; set; }
        [Required]
        [StringLength(32)]
        public string imageName { get; set; }
        [StringLength(256)]
        public string imageDescription { get; set; }
        public string fileType { get; set; }           // jpeg, svg, png, etc 
        [Required]
        public string imageUri { get; set; }
        public int imageWidth { get; set; }
        public int imageHeight { get; set; }

        //TODO remove this when possible
        //[Required]
        //public bool isDefault { get; set; }

              
    }

    //public class BugPicture : Image
    //{
    //    public virtual Bug BugModel {get;set;}
    //}

    //public class PlantPicture : Image
    //{
    //    public virtual Plant PlantModel { get; set; }
    //}

    public class BugSprite : Image
    {
        //public virtual Bug BugModel { get; set; }

        public Boolean isSheet { get; set; }

        public int horizontalFrames { get; set; }

        public int verticalFrames { get; set; }

        public bool bugMatchInd { get; set; }

        //Used for both bug sort and bug zap
        public bool bugSortInd { get; set; }
    }

    public class PlantSprite : Image
    {
        public bool plantMatchInd { get; set; }

        public bool fruitVeggieInd { get; set; }
    }

}