namespace JMG.Models
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;


    public class Instructor : ApplicationUser
    {

        public int InstructorID { get; set; }
        [Required]
        [Display(Name = "User name")]
        [StringLength(30)]
        public string userName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50)]
        public string FirstName{ get; set; }


        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool isDeleted { get; set; }
        public bool isAdmin { get; set; }
        public virtual ICollection<Student> Students { get; set; }          // navigation property 
    }





    public class Student : ApplicationUser
    {
        
        public int StudentID { get; set; }                      // pk for student 
        [Required]
        public string identityCode { get; set; }                // Whatever the code the teacher uses to identify the kid
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool isDeleted { get; set; }

       // public virtual ICollection<Assessment> Assessments { get; set; }          // source of error? 

    }


    public class UserModels : DbContext
    {

        public virtual DbSet<Instructor> Instructors { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        // Your context has been configured to use a 'UserModels' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'JMG.Models.UserModels' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'UserModels' 
        // connection string in the application configuration file.
        public UserModels()
            : base("name=UserModels")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    
}