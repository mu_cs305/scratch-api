﻿namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;


    //Represents a single Student
    public class Location
    {
        [Key]
        [Required]
        public int locationID { get; set; }

        [Required]
        public string name { get; set; }        

        [Required]
        public bool active { get; set; }
    }

}