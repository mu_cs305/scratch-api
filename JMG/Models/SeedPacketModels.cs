using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace JMG.Models
{
    // Represents a seed packet.  Each seed packet has a Soil type, a SunExposure, and a Plant
    public class SeedPacket
    {
        public SeedPacket()
        {

        }

        [Key]
        public int seedPacketID { get; set; }
        // This exists so that I can search for seed packets in the context.cs 
        public string nickname { get; set; }

        [StringLength(256)]
        public string assetUrlFront { get; set; }
        [StringLength(256)]
        public string assetUrlBack { get; set; }

        //public int Id { get; set; }
        public virtual Plant plant { get; set; }

        public virtual Soil soil { get; set; }

        public virtual SunExposure sunExposure { get; set; }

        //One side of a many-to-many relationship
        [JsonIgnore] //Ignore this property during Serialization
        public virtual ICollection<Assessment> assessments { get; set; }

    }
}


