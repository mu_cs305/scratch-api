﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMG.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        public Boolean isDeleted { get; set; }

        [Required]
        public DateTime dateCreated { get; set; }
        
        [Required]
        public Boolean isAdmin { get; set; }
    }

    public class UserNameModel
    {
        [Required]
        public string UserName { get; set; }
    }

    public class UserIdModel
    {
        [Required]
        public string id { get; set; }
    }

    public class PasswordChangeModel
    {
        [Required]
        public string id { get; set; }
        [Required]
        public string oldPass { get; set; }
        [Required]
        public string newPass { get; set; }
    }
}