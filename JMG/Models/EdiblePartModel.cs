namespace JMG.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    // Represents the Edible Part of a Plant
    // @TODO: this system may need revision as we progress
    public class EdiblePart
    {
        [Key]
        public int plantPartID { get; set; }
        [Required]
        [StringLength(20)]
        public string plantPartName { get; set; }
        [StringLength(256)]
        public string assetUrl { get; set; }



    }
}