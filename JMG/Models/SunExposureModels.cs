namespace JMG.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    //Represents a type of Sun Exposure
    public class SunExposure
    {
        [Key]
        public int sunExposureID { get; set; }
        [StringLength(50)]
        public string exposureName { get; set; }
        [StringLength(256)]
        public string assetUrl { get; set; }

    }

}