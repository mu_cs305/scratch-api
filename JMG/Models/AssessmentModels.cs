namespace JMG.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    // Represents the content of a test
    public class Assessment
    {
        [Key]
        public int assessmentID { get; set; }

        [StringLength(300)]
        public string description { get; set; }

        public DateTime dateCreated { get; set; }
  
        //One side of a many-to-many relationship
        public virtual ICollection<Bug> bugs { get; set; }
        //One side of a many-to-many relationship
        public virtual ICollection<SeedPacket> seedPackets {get; set;}

        //This was made obsolete by the addition of test locations.  If a location is inactive, then the corresponding assessment should also be inactives
        public bool isDeleted { get; set; }

        public virtual Location location { get; set; }
    }
}