namespace JMG.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class AssessmentBug
    {
        [Key]
        public int ID { get; set; }
        public virtual Assessment assessmentID { get; set; }
        public virtual Bug bugID { get; set; }
    }

    public class AssessmentBugModels : DbContext
    {
        // Your context has been configured to use a 'AssessmentBugModels' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'JMG.Models.AssessmentBugModels' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'AssessmentBugModels' 
        // connection string in the application configuration file.
        public DbSet<AssessmentBug> AssessmentBugs { get; set; }
        public AssessmentBugModels()
            : base("name=JMGConnection")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}