﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Linq;

namespace JMG.Models
{
    // Represents a Plant
    public class Plant
    {

        public Plant()
        {

        }

        [Key]
        public int PlantId { get; set; }
             
        [Required]
        [Column("PlantName")]
        [StringLength(50)]
        public string plantName { get; set; }
        public bool isHotSeasonPlant { get; set; }
        public bool isColdSeasonPlant { get; set; }
        [Required]
        public bool isFruit { get; set; }
        
        public virtual ICollection<PlantSprite> Images { get; set; }

    }
}