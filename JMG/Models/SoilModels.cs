﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JMG.Models
{
    // Represents a type of Soil
    public class Soil
    {
        [Key]
        public int SoilID { get; set; }
        [Required]
        [StringLength(50)]
        public string soilName { get; set; }
        [StringLength(256)]
        public string soilDescription { get; set; }
        [StringLength(256)]
        public string assetUrl { get; set; }
    }
}