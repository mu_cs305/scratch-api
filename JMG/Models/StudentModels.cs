namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;


    //Represents a single Student
    public class Student
    {
        [Key]
        [Required]
        [StringLength(6)]             
        public string studentID { get; set; }        

        [Required]
        public DateTime registrationDate { get; set; }

        //Each student has 0 or more test sessions
        public virtual ICollection<TestSession> testSessions { get; set; }
        
        [Required]
        public Boolean isDeleted { get; set; }
    }
    
}