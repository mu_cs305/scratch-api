﻿namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    // Represents the settings to the game
    // There will only be one instance of this model
    public class GameSettings
    {
        [Key]
        public int id { get; set; }

        [Required]
        [StringLength(4)]
        public string pin { get; set; }

        [Required]
        public Boolean remindPin { get; set; }

        [Required]
        public Boolean requirePin { get; set; }
    }
}