namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    // Represents an "Answer" that a student gives while taking a test
    public class Answer
    {
        [Key]
        public int answerID { get; set; }

        //Lets use the question field instead of description
        //[Required]
        //[Column("Description")]
        //[Display(Name = "Description")]
        //[StringLength(256)]
        //public string description { get; set; }
        
       
        [Required]
        [Column("StudentChoice")]
        [Display(Name ="StudentChoice")]
        [StringLength(256)]                              // 256 is long enough for long links to image and media resources
        public string studentChoice { get; set; }
     
        [Required]
        [Column("Question")]
        [StringLength(256)]                             
        public string question { get; set; }         

        [Required]
        [Column("CorrectChoice")]
        [StringLength(256)]
        public string correctChoice { get; set; }

        [Required]
        [Column("Chapter Number")]
        public int chapter { get; set; }

        [Required]
        [Column("TimeStamp")]
        public DateTime timestamp{ get; set; }

        [Required]
        //Will contain comma-separated string of choices
        public string choices { get; set; }
    }
}