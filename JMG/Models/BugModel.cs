namespace JMG.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    // Represents a single Bug object
    public class Bug
    {
        public Bug()
        {

        }

        [Key]
        public int BugID { get; set; }

        [Required]
        [Column("BugName")]
        [StringLength(50)]
        public string bugName { get; set; }

        [Required]
        public BugType BugType { get; set; }    


        //One side of a many-to-many relationship - WE NEED THIS
        [JsonIgnore] //Ignore this property during Serialization
        public virtual ICollection<Assessment> assessments { get; set; }

        public virtual ICollection<BugSprite> Images { get; set; }

    }

   

    public enum BugType
    {
        Predator,
        Pollinator,
        Pooper,
        Pest
    }
    
}