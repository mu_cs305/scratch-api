﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace JMG.Models
{
    // Represents a part of a plant
    public class PlantPart
        
    {
        [Key]
        public int plantPartID { get; set; }
        [Required]
        [StringLength(20)]
        public string plantPartName { get; set; }
        [StringLength(256)]
        public string assetUrl { get; set; } 
    
    }

}