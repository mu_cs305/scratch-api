namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;


    public class TestSession
    {
        [Key]
        [Required]
        public int testSessionId { get; set; }
        
        [Required]        
        public DateTime assessmentDate { get; set; }

        [Required]
        public DateTime startTime { get; set; }
        public DateTime completionTime { get; set; }

        // Model with a list of Answers 
        // One-to-Many so we don't need anything on the Answers side
        [Required]
        public virtual ICollection<Answer> answers { get; set; }
        // This should be one-to-one
        //@TODO: may require further revision
        [Required]
        public virtual Assessment assessment { get; set; }
    }

    public class TestSessionPostObject
    {
        public Student student { get; set; }
        public TestSession session { get; set; }
    }
}