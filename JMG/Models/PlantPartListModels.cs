namespace JMG.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    // @TODO: this may need revision as we continue.  We might be able to remove this in favor of many-to-many relationships
    public class PlantPartList
    {
        [Key]
        public int plantPartListID { get; set; }

        [StringLength(50)]
        public string plantPartListDescription { get; set; }

        public virtual ICollection<PlantPart> plantPart { get; set; }

    }
}