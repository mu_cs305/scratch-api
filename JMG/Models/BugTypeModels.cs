namespace JMG.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class BugType
    {
        [Key]
        public int bugTypeID { get; set; }
        [Required]
        public string typeName { get; set; }


        public virtual ICollection<Bug> Bugs { get; set; }
    }



    public class BugTypeModels : DbContext
    {
        public DbSet<BugType> BugTypes { get; set; }

        // Your context has been configured to use a 'BugTypeModels' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'JMG.Models.BugTypeModels' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'BugTypeModels' 
        // connection string in the application configuration file.
        public BugTypeModels()
            : base("name=JMGConnection")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}