namespace JMG.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using JMG.Models;
    using System.Collections.Generic;


    internal sealed class Configuration : DbMigrationsConfiguration<JMG.DAL.JMGContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        // DB MIGRATION TROUBLESHOOTING
        // 1) If you get errors when running update-database,
        // make sure that all the tables exist in the DB before 
        // running Seed() ; comment stuff out if you have to

        // 2) If you're unable to add a migration despite starting "fresh", 
        // be sure to remove the .mdf and log file from App_Data; for some reason
        // entity will try tin include that in its migrations 

        // 3) If nothing else works, turn VS off and turn it back on again. 
        // The amount of times this has resolved issues is TOO DAMN HIGH


        /*
        Here's how ICollection foreign keys should be handled here 

            courses[0].Instructors.Add(instructors[0]);
            courses[0].Instructors.Add(instructors[1]);
            courses[1].Instructors.Add(instructors[2]);

            context.SaveChanges();

       */



        protected override void Seed(DAL.JMGContext context)
        {
            base.Seed(context);
            //     base.Seed(context);

            //Seed the default game settings
            GameSettings settings = new GameSettings
            {
                pin = "1234",
                remindPin = true,
                requirePin = true
            };
            context.GameSettings.Add(settings);


            //Seed a student
            var stud = new Student
            {
                studentID = "stud01",
                registrationDate = DateTime.Now,
                testSessions = new List<TestSession>(),
                isDeleted = false
            };
            context.Students.Add(stud);
            context.SaveChanges();

            var plantParts = new List<PlantPart>
            {
                new PlantPart {plantPartName = "Seed" },
                new PlantPart {plantPartName = "Fruit" },
                new PlantPart {plantPartName = "Leaves" },
                new PlantPart {plantPartName = "Roots" },
                new PlantPart {plantPartName = "Stem" },
                new PlantPart {plantPartName = "Flowers" },
            };
            plantParts.ForEach(plant => context.PlantParts.Add(plant));
            context.SaveChanges();


            var ediblePartsList = new List<EdiblePart>
            {
                new EdiblePart {plantPartName = "Seed" },
                new EdiblePart {plantPartName = "Fruit" },
                new EdiblePart {plantPartName = "Leaves" },
                new EdiblePart {plantPartName = "Roots" },
                new EdiblePart {plantPartName = "Stem" },
                new EdiblePart {plantPartName = "Flowers" },
            };
            ediblePartsList.ForEach(part => context.EdibleParts.Add(part));
            context.SaveChanges();



            Plant beets = new Plant
            {
                plantName = "Beets",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(beets);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Beets").Images = new List<PlantSprite>
            {
                new PlantSprite
                {
                    
                    imageName = "Beet pic",
                    imageDescription = "Just some beets",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200/v1458752170/Beets.jpg",
                    imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },
            };
            context.SaveChanges();

            Plant bberry = new Plant
            {
                plantName = "Blueberries",
                isColdSeasonPlant = false,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(bberry);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Blueberries").Images = new List<PlantSprite>
            {
                new PlantSprite
                {
                    
                    imageName = "Blueberries",
                    imageDescription = "Just some blueberries",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200/v1458752045/Blueberries.jpg",
                    imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },

            };

            context.SaveChanges();
            Plant broc = new Plant
            {
                plantName = "Broccoli",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(broc);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Broccoli").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Broccoli",
                    imageDescription = "Cabbage with a crown",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200/v1458752108/Broccoli.jpg",
                    imageWidth = 192,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },
            };
            context.SaveChanges();

            Plant crt = new Plant
            {
                plantName = "Carrots",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,

            };
            context.Plants.Add(crt);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Carrots").Images = new List<PlantSprite>
            {
                new PlantSprite
                {
                     
                    imageName = "Carrot",
                    imageDescription = "They weren't always orange",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_60,r_9/v1458752332/Carrots.jpg",
                    imageWidth = 380,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },
            };
            context.SaveChanges();

            Plant caul = new Plant
            {
                plantName = "Cauliflower",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(caul);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Cauliflower").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Cauliflower",
                    imageDescription = "Something about cauliflower",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_48,r_0/v1458752406/Cauliflower.jpg",
                    imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },
            };
            context.SaveChanges();

            Plant collardGreens = new Plant
            {
                plantName = "Collared Greens",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(collardGreens);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Collared Greens").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Collared Greens",
                    imageDescription = "By the collar",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_54/v1458752476/Collared%20Greens.jpg",
                    imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true
                },
            };
            context.SaveChanges();

            Plant corn = new Plant
            {
                plantName = "Corn",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(corn);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Corn").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Corn",
                    imageDescription = "Something something corn",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_40/v1458752544/Corn.jpg",
                    imageWidth = 256,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();



            Plant cucumber = new Plant
            {
                plantName = "Cucumber",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(cucumber);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Cucumber").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Cucumber",
                    imageDescription = "Something something cucumber",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_40/a_0/v1458752682/Cucumber.jpg",
                    imageWidth = 255,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();


            Plant eggPlant = new Plant
            {
                plantName = "Eggplant",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(eggPlant);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Eggplant").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Eggplant",
                    imageDescription = "Something something eggplant",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_40/v1458752752/Eggplant.jpg",
                     imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();


            Plant greenBeans = new Plant
            {
                plantName = "Green Bean",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(greenBeans);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Green Bean").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Green Bean",
                    imageDescription = "Something something green bean",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458752854/Green Beans.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant lettuce = new Plant
            {
                plantName = "Lettuce",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(lettuce);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Lettuce").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Lettuce",
                    imageDescription = "Something something lettuce",
                    fileType = ".jpg",
                    imageUri ="https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458752920/Lettuce.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();


            Plant jalapeno = new Plant
            {
                plantName = "Jalapeno Pepper",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(jalapeno);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Jalapeno Pepper").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Jalapeno Pepper",
                    imageDescription = "Something something jalapeno pepper",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458752991/Jalapeno%20Pepper.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant okra = new Plant
            {
                plantName = "Okra",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(okra);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Okra").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Okra",
                    imageDescription = "Something something okra",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753043/Okra.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant onion = new Plant
            {
                plantName = "Onion",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(onion);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Onion").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Onion",
                    imageDescription = "Something something onion",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753126/Onion.jpg",
                     imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant peas = new Plant
            {
                plantName = "Peas",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(peas);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Peas").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Peas",
                    imageDescription = "Something something peas",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458753200/Peas.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();


            Plant pepper = new Plant
            {
                plantName = "Pepper",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(pepper);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Pepper").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Pepper",
                    imageDescription = "Something something pepper",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753259/Pepper.jpg",
                    imageWidth = 278,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant potato = new Plant
            {
                plantName = "Potato",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(potato);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Potato").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Potato",
                    imageDescription = "Something something potato",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753402/Potato.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant pumpkin = new Plant
            {
                plantName = "Pumpkin",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(pumpkin);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Pumpkin").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Pumpkin",
                    imageDescription = "Something something pumpkin",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753479/Pumpkin.jpg",
                    imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant radish = new Plant
            {
                plantName = "Radish",
                isColdSeasonPlant = true,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(radish);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Radish").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Radish",
                    imageDescription = "Something something radish",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753530/Radish.jpg",
                     imageWidth = 200,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant strawBerry = new Plant
            {
                plantName = "Strawberry",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(strawBerry);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Strawberry").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Strawberry",
                    imageDescription = "Something something strawberry",
                    fileType = ".jpg",
                    imageUri ="https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753582/Strawberry.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant tomato = new Plant
            {
                plantName = "Tomato",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(tomato);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Tomato").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Tomato",
                    imageDescription = "Something something tomato",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753654/Tomato.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant squash = new Plant
            {
                plantName = "Squash",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(squash);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Squash").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Squash",
                    imageDescription = "Something something squash",
                    fileType = ".jpg",
                    imageUri ="https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753834/Sqush.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();

            Plant watermelon = new Plant
            {
                plantName = "Watermelon",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(watermelon);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Watermelon").Images = new List<PlantSprite>
            {
                new PlantSprite {
                    
                    imageName = "Watermelon",
                    imageDescription = "Something something watermelon",
                    fileType = ".jpg",
                    imageUri ="https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458753896/Watermelon.jpg",
                    imageWidth = 300,
                    imageHeight = 200,
                    plantMatchInd = true,
                    fruitVeggieInd = true

                },
            };
            context.SaveChanges();


            ////////////////////////// BEGIN BUGS /////////////////////
            //Bug ant = new Bug
            //{
            //    bugName = "Ant",
            //    BugType = BugType.Pest,
            //};

            //context.Bugs.Add(ant);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Ant").Images = new List<BugSprite> {
            //            new BugSprite
            //            {
            //                
            //                imageName = "Ant",
            //                imageDescription = "This insect sometimes goes marching",
            //                fileType = ".jpg",
            //                imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448119474/ant-564617_1280_if3cqc.jpg",
            //                imageWidth = 600,
            //                imageHeight = 800,
            //            },
            //              new BugSprite
            //                    {
            //                        
            //                        imageName = "European wasp",
            //                        imageDescription = "Sprite for a euro wasp",
            //                        fileType = ".gif",

            //                        // Don't use this one in production! 
            //                        imageUri = "http://heavycatstudios.com/resource/1002-0017.gif",
            //                        imageWidth = 250,
            //                        imageHeight = 250,

            //                    },
            //        };
            //context.SaveChanges();

            //Aphid
            Bug aphid = new Bug
            {
                bugName = "Aphid",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(aphid);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Aphid").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Aphid",
                        imageDescription = "Nothing funny about aphids",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458797430/Aphid.jpg",
                        imageWidth = 200,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Aphid Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455870722/aphid_sprite_ljuron.png",
                        imageWidth = 400,
                        imageHeight = 200,
                        isSheet = true,
                        horizontalFrames = 4,
                        verticalFrames = 2,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bee
            Bug bee = new Bug
            {
                bugName = "Bee",
                BugType = BugType.Pollinator,
            };
            context.Bugs.Add(bee);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Bee").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Bee Image",
                        imageDescription = "Buzzz - a nice disc",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458754129/Bee.jpg",
                        imageWidth = 299,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Bee Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869889/bee_sheet_vcpq3j.png",
                        imageWidth = 910,
                        imageHeight = 700,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Butterfly
            Bug butterfly = new Bug
            {
                bugName = "Butterfly",
                BugType = BugType.Pollinator,
            };
            context.Bugs.Add(butterfly);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Butterfly").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Butterfly Image",
                        imageDescription = "Wingy thingies",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458797507/Butterfly.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Butterfly Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455871007/butterfly-sheet_ozjahw.png",
                        imageWidth = 1270,
                        imageHeight = 1040,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Cabbage Looper
            Bug cLooper = new Bug
            {
                bugName = "Cabbage Looper",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(cLooper);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Cabbage Looper").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Cabbage Looper Image",
                        imageDescription = "Generic image",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_30/v1458797573/Cabbage%20Looper.jpg",
                        imageWidth = 351,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Cabbage Looper Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869915/cabbage_looper_sheet_pxew7g.png",
                        imageWidth = 440,
                        imageHeight = 970,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Corn Earworm
            Bug cEarworm = new Bug
            {
                bugName = "Corn Earworm",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(cEarworm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Corn Earworm").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Corn Earworm Image",
                        imageDescription = "Generic image",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458797604/Corn Earworm.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Corn Earworm Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869910/corn_earworm_sheet_de1pbv.png",
                        imageWidth = 255,
                        imageHeight = 925,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Cucumber Beetle
            Bug cBeetle = new Bug
            {
                bugName = "Cucumber Beetle",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(cBeetle);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Cucumber Beetle").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Cucumber Beetle Image",
                        imageDescription = "Yellow ladybug",
                        fileType = ".jpg",
                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458797640/Cucumber%20Beetle.jpg",
                        imageWidth =  300,
                        imageHeight = 200,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Cucumber Beetle Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869902/cucumber_beetle_sheet_l6yfhk.png",
                        imageWidth = 685,
                        imageHeight = 690,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Dung Beetle
            Bug dBeetle = new Bug
            {
                bugName = "Dung Beetle",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(dBeetle);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Dung Beetle").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Beetle Photo",
                        imageDescription = "Poo Ball",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_23/v1458797663/Dung Beetle.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Beetle Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869897/dung_beetle_sheet_haaxbo.png",
                        imageWidth = 845,
                        imageHeight = 1010,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //earthworm
            Bug earthworm = new Bug
            {
                bugName = "Earthworm",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(earthworm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Earthworm").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Earthworm Photo",
                        imageDescription = "Nature's squirmy soil aerator",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/q_20/v1458797704/Earthworm.jpg",
                        imageWidth = 200,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Earthworm Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/a_270/v1455871952/earthworm_sprite_opxqc4.png",
                        imageWidth = 200,
                        imageHeight = 400,
                        isSheet = true,
                        horizontalFrames = 2,
                        verticalFrames = 4,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bug earwig = new Bug
            //{
            //    bugName = "Earwig",
            //    BugType = BugType.Pest,
            //};
            //context.Bugs.Add(earwig);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Earwig").Images = new List<BugSprite>
            //{
            //     new BugSprite
            //                    {
            //                        
            //                        imageName = "Earwig",
            //                        imageDescription = "Not a hairpiece for your ears.",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/earwig-560780_1280_qgaj1k.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //Bug fly = new Bug
            //{
            //    bugName = "Fly",
            //    BugType = BugType.Pooper,
            //};
            //context.Bugs.Add(fly);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Fly").Images = new List<BugSprite>
            //{
            //     new BugSprite
            //                    {
            //                        
            //                        imageName = "Fly",
            //                        imageDescription = "Makes a buzz nobody loves to hear.",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048627/fly-946712_1280_mcx1n1.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //Bug gBeetle = new Bug
            //{
            //    bugName = "Ground Beetle",
            //    BugType = BugType.Predator,
            //};
            //context.Bugs.Add(gBeetle);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Ground Beetle").Images = new List<BugSprite>
            //{
            //     new BugSprite
            //                    {
            //                        
            //                        imageName = "Ground Beetle",
            //                        imageDescription = "This bug keeps its nose to the ground.",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448119698/ground_beetle-2821B_jt4hnk.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //grub worm
            Bug gWorm = new Bug
            {
                bugName = "Grub Worm",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(gWorm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Grub Worm").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Grub Worm Photo",
                        imageDescription = "Just a grub worm",
                        fileType = ".jpg",
                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_23/v1458797779/Grub%20Worm.jpg",
                        imageWidth = 200,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Grub Worm Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/c_scale,w_1140/v1455869894/grub_worm_sheet_sfdqlo.png",
                        imageWidth = 1140,
                        imageHeight = 1300,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bug jBeetle = new Bug
            //{
            //    bugName = "Japanese Beetle",
            //    BugType = BugType.Pest,
            //};
            //context.Bugs.Add(jBeetle);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Japanese Beetle").Images = new List<BugSprite>
            //{
            //    new BugSprite
            //                    {
            //                        
            //                        imageName = "Japanese Beetle",
            //                        imageDescription = "Big in Japan and in America.",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048636/japanese_beetle_fbeurf.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //lacewing
            Bug lacewing = new Bug
            {
                bugName = "Lacewing",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(lacewing);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Lacewing").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Lacewing Photo",
                        imageDescription = "Just a Lacewing",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_23/v1458797828/Lacewing.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false

                    },
                    new BugSprite
                    {
                        
                        imageName = "Lacewing Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,w_1000/v1460991977/small_lacewing_sheet_wwqiki.png",
                        imageWidth = 1000,
                        imageHeight = 1332,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //ladybug
            Bug ladybug = new Bug
            {
                bugName = "Ladybug",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(ladybug);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Ladybug").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Ladybug Photo",
                        imageDescription = "Just a Ladybug",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458797874/Ladybug.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Ladybug Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/c_limit,w_1000/v1460919922/ladybug_sheet_lsamqf.png",
                        imageWidth = 1000,
                        imageHeight = 1000,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //mealybug
            Bug mealybug = new Bug
            {
                bugName = "Mealy Bug",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(mealybug);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Mealy Bug").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Mealy Bug Photo",
                        imageDescription = "The Hell is a Mealy Bug?",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/q_20/v1458797910/Mealy%20Bug.jpg",
                        imageWidth = 300,
                        imageHeight = 200,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Mealy Bug Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_limit,w_1000/v1460918306/small_mealybug_sheet_lqb1sx.png",
                        imageWidth = 1000,
                        imageHeight = 1000,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bug midge = new Bug
            //{
            //    bugName = "Midge",
            //    BugType = BugType.Pollinator,
            //};
            //context.Bugs.Add(midge);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Midge").Images = new List<BugSprite>
            //{
            //       new BugSprite
            //                    {
            //                        
            //                        imageName = "Midge",
            //                        imageDescription = "Something pithy here",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048628/fly-640860_1280_muuoqj.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //Bug moth = new Bug
            //{
            //    bugName = "Moth",
            //    BugType = BugType.Pollinator,
            //};
            //context.Bugs.Add(moth);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Moth").Images = new List<BugSprite>
            //{
            //     new BugSprite
            //                    {
            //                        
            //                        imageName = "Moth",
            //                        imageDescription = "Goes towards the light",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048629/moth-238937_1280_bkapb4.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();


            //Bug nem = new Bug
            //{
            //    bugName = "Nematode",
            //    BugType = BugType.Pooper,
            //};
            //context.Bugs.Add(nem);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Nematode").Images = new List<BugSprite>
            //{
            //    new BugSprite
            //                    {
            //                        
            //                        imageName = "Nematode",
            //                        imageDescription = "Nothing to do with amphibians",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048628/nematode_ikhkcr.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            //praying mantis
            Bug pm = new Bug
            {
                bugName = "Praying Mantis",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(pm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Praying Mantis").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Praying Mantis Photo",
                        imageDescription = "Pray Tell",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458797968/Praying%20Mantis.jpg",
                        imageWidth = 200,
                        imageHeight = 300,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Praying Mantis Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/c_scale,w_1565/v1455869863/praying_mantis_sheet_f3jaku.png",
                        imageWidth = 1565,
                        imageHeight = 1765,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();


            //pill bug
            Bug pb = new Bug
            {
                bugName = "Pill Bug",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(pb);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Pill Bug").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Pill Bug Photo",
                        imageDescription = "Don't swallow a pill bug",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458797995/Pill Bug.jpg",
                        imageWidth = 200,
                        imageHeight = 300,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Pill Bug Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,w_1000/v1460919110/small_pillbug_sheet_nlryly.png",
                        imageWidth = 1000,
                        imageHeight = 1000,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bug slug = new Bug
            //{
            //    bugName = "Slug",
            //    BugType = BugType.Pest,
            //};
            //context.Bugs.Add(slug);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Slug").Images = new List<BugSprite>
            //{
            //        new BugSprite
            //                    {
            //                        
            //                        imageName = "Slug",
            //                        imageDescription = "Leaves a shiny trail",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048635/tigerschnecke-332165_1280_ba5era.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },

            //};
            //context.SaveChanges();

            //Bug snail = new Bug
            //{
            //    bugName = "Snail",
            //    BugType = BugType.Pest,
            //};
            //context.Bugs.Add(snail);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Snail").Images = new List<BugSprite>
            //{
            //           new BugSprite
            //                    {
            //                        
            //                        imageName = "Snail",
            //                        imageDescription = "The pace-setter",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/snails-382992_1280_a0bfeo.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();


            //Bug spider = new Bug
            //{
            //    bugName = "Spider",
            //    BugType = BugType.Predator,
            //};
            //context.Bugs.Add(spider);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Spider").Images = new List<BugSprite>
            //{
            //                    new BugSprite
            //                    {
            //                        
            //                        imageName = "Spider",
            //                        imageDescription = "Our pal the arachnid",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048631/spider-564685_1280_mlyadv.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },

            //};
            //context.SaveChanges();

            //Spider Mite
            Bug sMite = new Bug
            {
                bugName = "Spider Mite",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(sMite);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Spider Mite").Images = new List<BugSprite>
            {
                   new BugSprite
                    {
                        
                        imageName = "Spider Mite Photo",
                        imageDescription = "Don't swallow a Spider Mite either",
                        fileType = ".jpg",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458798020/Spider Mite.jpg",
                        imageWidth = 200,
                        imageHeight = 300,
                        isSheet = false,
                        bugMatchInd = true,
                        bugSortInd = false
                    },
                    new BugSprite
                    {
                        
                        imageName = "Spider Mite Sprite",
                        imageDescription = "Spritesheet",
                        fileType = ".png",
                        imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_limit,w_1000/v1460918079/small_mite_sheet_toiosj.png",
                        imageWidth = 1000,
                        imageHeight = 1000,
                        isSheet = true,
                        horizontalFrames = 5,
                        verticalFrames = 5,
                        bugMatchInd = false,
                        bugSortInd = true
                    },
            };
            context.SaveChanges();

            //Bug stail = new Bug
            //{
            //    bugName = "Springtail",
            //    BugType = BugType.Pooper,
            //};
            //context.Bugs.Add(stail);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Springtail").Images = new List<BugSprite>
            //{
            //    new BugSprite
            //                    {
            //                        
            //                        imageName = "Springtail",
            //                        imageDescription = "Found in summer, too",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/springtail_giqqyq.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            ////Tomato Hornworm
            //Bug tHornworm = new Bug
            //{
            //    bugName = "Tomato Hornworm",
            //    BugType = BugType.Predator,
            //};
            //context.Bugs.Add(tHornworm);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Tomato Hornworm").Images = new List<BugSprite>
            //{
            //       new BugSprite
            //        {
            //            
            //            imageName = "Tomato Hornworm Photo",
            //            imageDescription = "This has to be made up",
            //            fileType = ".jpg",
            //            imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455873916/149777-850x436-Tomato-hornworm_x52n7y.jpg",
            //            imageWidth = 600,
            //            imageHeight = 800,
            //            isSheet = false
            //        },
            //        new BugSprite
            //        {
            //            
            //            imageName = "Tomato Hornworm Sprite",
            //            imageDescription = "Spritesheet",
            //            fileType = ".png",
            //            imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869868/mite_sheet_uhk5mm.png",
            //            imageWidth = 1430,
            //            imageHeight = 1280,
            //            isSheet = true,
            //            horizontalFrames = 5,
            //            verticalFrames = 5
            //        },
            //};
            //context.SaveChanges();

            Bug svb = new Bug
            {
                bugName = "Squash Vine Borer",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(svb);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Squash Vine Borer").Images = new List<BugSprite>
            {
                new BugSprite
                {
                    
                    imageName = "Squah vine borer",
                    imageDescription = "A boring bug",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458798061/Squash%20Vine%20Borer.jpg",
                    imageWidth = 200,
                    imageHeight = 300,
                    isSheet = false,
                    bugMatchInd = true,
                    bugSortInd = false
                },
                new BugSprite
                {
                    
                    imageName = "Squah vine borer sheet",
                    imageDescription = "Spritesheet",
                    fileType = ".png",
                    imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/c_scale,w_1440/v1455869859/squash_vine_borer_sheet_v3zl7y.png",
                    imageWidth = 1440,
                    imageHeight = 1540,
                    isSheet = true,
                    horizontalFrames = 5,
                    verticalFrames = 5,
                    bugMatchInd = false,
                    bugSortInd = true
                },
            };
            context.SaveChanges();

            //Bug sb = new Bug
            //{
            //    bugName = "Stink Bug",
            //    BugType = BugType.Pest,
            //};
            //context.Bugs.Add(sb);
            //context.SaveChanges();
            //context.Bugs.FirstOrDefault(s => s.bugName == "Stink Bug").Images = new List<BugSprite>
            //{
            //     new BugSprite
            //                    {
            //                        
            //                        imageName = "Stink Bug",
            //                        imageDescription = "Kind of a stinker",
            //                        fileType = ".jpg",
            //                        imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048633/stink_bug_pyzx1v.jpg",
            //                        imageWidth = 600,
            //                        imageHeight = 800,
            //                    },
            //};
            //context.SaveChanges();

            Bug wasp = new Bug
            {
                bugName = "Wasp",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(wasp);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Wasp").Images = new List<BugSprite>
            {
                new BugSprite
                {
                    
                    imageName = "European wasp",
                    imageDescription = "A real stringer",
                    fileType = ".jpg",
                    imageUri = "https://res.cloudinary.com/duyfjnu2h/image/upload/c_scale,h_200,q_20/v1458798112/Wasp.jpg",
                    imageWidth = 600,
                    imageHeight = 800,
                    isSheet = false,
                    bugMatchInd = true,
                    bugSortInd = false
                },
                new BugSprite
                {
                    
                    imageName = "Wasp Spritesheet",
                    imageDescription = "Spritesheet",
                    fileType = ".png",
                    imageUri = "http://res.cloudinary.com/dxwzkfuvu/image/upload/v1455869939/wasp_sheet_scaxwo.png",
                    imageWidth = 400,
                    imageHeight = 200,
                    isSheet = false,
                    horizontalFrames = 4,
                    verticalFrames = 2,
                    bugMatchInd = false,
                    bugSortInd = true
                },
            };
            context.SaveChanges();


            /* Plant list 
                    Beets, Broccoli, Carrots, Cauliflower, 
                    Collared Greens, Corn, Cucumber, Eggplant,
                    Green Beans, Lettuce, Jalapeno Pepper,
                    Okra, Onion, Peas, Pepper, Potato, Pumpkin,
                    Radish, Tomato, Squash, Watermelon
                */

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Burpee Beet Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Beets"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Burpee Broccoli Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Broccoli"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Burpee Carrot Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Carrots"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Cauliflower Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Cauliflower"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "American Seed Collared Green Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Collared Greens"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Corn Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Corn"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Cucumber Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Cucumber"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Hart's Eggplant Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Eggplant"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Burpee Organic Green Bean Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Green Bean"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Flashy Butter Oak Lettuce Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Lettuce"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Hart's Jalapeno Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Jalapeno Pepper"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Okra Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Okra"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "High Mowing Organic Onion Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Onion"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Dwarf Grey Sugar Pea Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Peas"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Hart's California Wonder Pepper Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Pepper"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Seed Potatoes",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Potato"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Winter Luxury Pie Pumpkin Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Pumpkin"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Radish Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Radish"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Baker Creek Mortgage Lifter Tomato Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Tomato"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Hart's Squash Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Squash"),
            });

            context.SeedPackets.Add(new SeedPacket
            {
                nickname = "Burpee Watermelon Seeds",
                plant = context.Plants.FirstOrDefault(s => s.plantName == "Watermelon"),
            });

            context.SaveChanges();

            //Seed the locations
            //TODO: get the actual names of the locations
            context.Locations.Add(new Location
            {
                name = "ADL",
                active = true
            });


            context.Locations.Add(new Location
            {
                name = "SHE",
                active = true
            });


            context.Locations.Add(new Location
            {
                name = "FFE",
                active = true
            });

            context.SaveChanges();

            //  Seed an assessment for each location
            Location[] locations = context.Locations.ToArray();
            foreach(Location loc in locations)
            {
                List<Bug> bugList = context.Bugs.ToList();
                List<SeedPacket> seedList = context.SeedPackets.ToList();

                Assessment assessment = new Assessment
                {
                    description = "The assessment for " + loc.name,
                    dateCreated = DateTime.Now,
                    isDeleted = false,
                    bugs = bugList,
                    seedPackets = seedList,
                    location = loc
                };

                context.Assessments.Add(assessment);
           
            }

            //List<Bug> bugList = new List<Bug>();
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 1));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 2));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 3));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 4));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 5));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 6));
            //bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 7));

            //List<SeedPacket> packetList = new List<SeedPacket>();
            //packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 1));
            //packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 3));
            //packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 5));
            //Assessment seedAssessment = new Assessment
            //{
            //    description = "The default assessment.  Contains seven bugs and three seed packets.",
            //    dateCreated = DateTime.Now,
            //    isDeleted = false,
            //    bugs = bugList,
            //    seedPackets = packetList,
            //};
            //Assessment added = context.Assessments.Add(seedAssessment);
            //context.SaveChanges();
        }


    }
}
