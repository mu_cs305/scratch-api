﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;
using System.Data.SqlClient;

namespace JMG.Controllers
{
    public class AssessmentsController : ApiController
    {
        //private AssessmentModels db = new AssessmentModels();
        private JMGContext db = new JMGContext();

        // GET: api/Assessments
        public IQueryable<Assessment> GetAssessments()
        {
            return db.Assessments.Where(a => !a.isDeleted);
        }

        [Route("api/assessments/unfiltered")]
        public IQueryable<Assessment> GetAssessmentsUnfiltered()
        {
            return db.Assessments;
        }

        // GET: api/Assessments/5
        [ResponseType(typeof(Assessment))]
        public IHttpActionResult GetAssessment(int id)
        {
            Assessment assessment = db.Assessments.Find(id);
            if (assessment == null)
            {
                return NotFound();
            }

            return Ok(assessment);
        }

        // PUT: api/Assessments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssessment(int id, Assessment assessment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assessment.assessmentID)
            {
                return BadRequest();
            }

            //Screw LINQ, I'm doing this myself...

            //Update the basic assessment properties
            int locationId = assessment.location.locationID;
            string sql = "UPDATE Assessments SET "+
                "description=@desc, isDeleted=@isDeleted, location_locationID=@locID "+
                "WHERE assessmentID=" + id + ";";

            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@desc", assessment.description));
            parameterList.Add(new SqlParameter("@isDeleted", assessment.isDeleted));
            parameterList.Add(new SqlParameter("@locID", assessment.location.locationID));
            SqlParameter[] parameters = parameterList.ToArray();
            int result = db.Database.ExecuteSqlCommand(sql, parameters);

            //update the list of bugs:
            //First delete any existing relationships
            sql = "DELETE FROM BugAssessments WHERE Assessment_assessmentID = " + id;
            result = db.Database.ExecuteSqlCommand(sql);
            //Now add relationships for each included bug
            for(int i = 0; i < assessment.bugs.Count; i++)
            {
                Bug bug = assessment.bugs.ElementAt(i);
                sql = "INSERT INTO BugAssessments (Bug_BugID, Assessment_assessmentID) " +
                "VALUES (@bugId, @assessment_ID)";
                parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("@bugId", bug.BugID));
                parameterList.Add(new SqlParameter("@assessment_ID", assessment.assessmentID));
                parameters = parameterList.ToArray();
                result = db.Database.ExecuteSqlCommand(sql, parameters);
            }

            //update the list of plants:
            //First delete any existing relationships
            sql = "DELETE FROM SeedPacketAssessments WHERE Assessment_assessmentID = " + id;
            result = db.Database.ExecuteSqlCommand(sql);
            //Now add relationships for each included seed packet
            for (int i = 0; i < assessment.seedPackets.Count; i++)
            {
                SeedPacket sp = assessment.seedPackets.ElementAt(i);
                sql = "INSERT INTO SeedPacketAssessments (SeedPacket_seedPacketID, Assessment_assessmentID) " +
                "VALUES (@seedID, @assessment_ID)";
                parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("@seedID", sp.seedPacketID));
                parameterList.Add(new SqlParameter("@assessment_ID", assessment.assessmentID));
                parameters = parameterList.ToArray();
                result = db.Database.ExecuteSqlCommand(sql, parameters);
            }            

            return Ok(assessment);
        }

        // POST: api/Assessments
        [ResponseType(typeof(Assessment))]
        public IHttpActionResult PostAssessment(Assessment assessment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach(Bug b in assessment.bugs)
            {
                db.Bugs.Attach(b);
            }
            foreach(SeedPacket sp in assessment.seedPackets)
            {
                db.SeedPackets.Attach(sp);
            }

            db.Assessments.Add(assessment);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = assessment.assessmentID }, assessment);
        }

        // DELETE: api/Assessments/5
        [ResponseType(typeof(Assessment))]
        public IHttpActionResult DeleteAssessment(int id)
        {
            Assessment assessment = db.Assessments.Find(id);
            if (assessment == null)
            {
                return NotFound();
            }

            //db.Assessments.Remove(assessment);
            assessment.isDeleted = true;
            db.SaveChanges();

            return Ok(assessment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssessmentExists(int id)
        {
            return db.Assessments.Count(e => e.assessmentID == id) > 0;
        }
    }
}