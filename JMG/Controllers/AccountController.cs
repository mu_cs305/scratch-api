﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using JMG.Models;
using System.Web.Http;
using JMG.DAL;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Http;
using System.Net;

namespace JMG.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private AuthRepository _repo = null;
        
        public AccountController()
        {
            _repo = new AuthRepository();
        }

        // POST api/Account/Register
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            userModel.dateCreated = DateTime.Now;
            userModel.isDeleted = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterUser(userModel);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        [HttpPost]
        [ActionName("ChangePW")]
        [Route("changepw")]
        public IHttpActionResult ChangePassword(PasswordChangeModel model)
        {
            ApplicationUser user = null;
            try
            {
                user = _repo.UpdatePassword("" + model.id, model.oldPass, model.newPass);
            }
            catch(UserNotFoundException ex1)
            {
                return NotFound();
            }
            catch(PasswordMismatchException ex2)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The old password is not correct.")
                };
                throw new HttpResponseException(message);
            }
            catch(Exception ex)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("An error occurred changing the user's password.")
                };
                throw new HttpResponseException(message);
            }
            
            return Ok(user);
        }

        [Route("all")]
        public IQueryable<ApplicationUser> GetAll()
        {
            return _repo.getAllUsers();
        }

        [Route("test")]
        public IQueryable<ApplicationUser> GetAllTest()
        {
            return _repo.getAllUsers();
        }

        [Route("admins")]
        public IQueryable<ApplicationUser> GetAdmins()
        {
            return _repo.getAdmins();
        }

        [Route("instructors")]
        public IQueryable<ApplicationUser> GetInstructors()
        {
            return _repo.getInstructors();
        }

        [Route("all/unfiltered")]
        public IQueryable<ApplicationUser> GetAllUnfiltered()
        {
            //TODO: maybe convert this to async action
            return _repo.getAllUnfiltered();
        }

        [Route("user")]
        [AcceptVerbs("POST")]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult GetUser(UserIdModel ID)
        {
            //TODO: maybe convert this to async action
            string id = ID.id;
            ApplicationUser user = _repo.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }


        [Route("Self")]
        [AcceptVerbs("POST")]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult SelfByUserName(UserNameModel username)
        {
            //TODO: maybe convert this to async action
            string UserName = username.UserName;
            ApplicationUser user = _repo.GetUserByUsername(UserName);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [Route("user")]
        [AcceptVerbs("PUT")]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult PutUser(ApplicationUser user)
        {
            ApplicationUser modified = _repo.UpdateUser(user);
            if (modified == null)
            {
                return NotFound();
            }
            else return Ok(user);
        }

        [Route("user")]
        [AcceptVerbs("DELETE")]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult DeleteUser(UserIdModel id)
        {
            ApplicationUser deleted = _repo.ArchiveUser(id.id);
            if (deleted == null)
            {
                return NotFound();
            }
            else return Ok(deleted);
        }


          
        // @TODO REQUIRE SSL or HTTPS, because this is controller is a security disaster.
        [Route("ResetPassword")]
       // [AcceptVerbs("PUT")]
        [HttpPost]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult ResetPassword(UserIdModel user, string newPassword, string confirmNewPassword)
        {
            UserManager<IdentityUser> userManager =
                 new UserManager<IdentityUser>(new UserStore<IdentityUser>());

            if (user == null)
            {
                return NotFound();
            }    
            else if (newPassword != confirmNewPassword)
            {
                return BadRequest();            // this is the probably wrong error to return 
            }
            else
                userManager.RemovePassword(user.id);
                userManager.AddPassword(user.id, newPassword);
            return Ok();
        }
        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
