﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.DAL;
using JMG.Models;

namespace JMG.Controllers
{
    public class GameSettingsController : ApiController
    {
        private JMGContext db = new JMGContext();

        // GET: api/GameSettings
        public IQueryable<GameSettings> GetGameSettings()
        {
            return db.GameSettings;
        }

        // GET: api/GameSettings/5
        [ResponseType(typeof(GameSettings))]
        public IHttpActionResult GetGameSettings(int id)
        {
            GameSettings gameSettings = db.GameSettings.Find(id);
            if (gameSettings == null)
            {
                return NotFound();
            }

            return Ok(gameSettings);
        }

        // PUT: api/GameSettings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGameSettings(int id, GameSettings gameSettings)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gameSettings.id)
            {
                return BadRequest();
            }

            db.Entry(gameSettings).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameSettingsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(gameSettings);
        }

        // DELETE: api/GameSettings/5
        [ResponseType(typeof(GameSettings))]
        public IHttpActionResult DeleteGameSettings(int id)
        {
            GameSettings gameSettings = db.GameSettings.Find(id);
            if (gameSettings == null)
            {
                return NotFound();
            }

            db.GameSettings.Remove(gameSettings);
            db.SaveChanges();

            return Ok(gameSettings);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GameSettingsExists(int id)
        {
            return db.GameSettings.Count(e => e.id == id) > 0;
        }
    }
}