﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;

namespace JMG.Controllers
{
    public class SunExposuresController : ApiController
    {
        //private SunExposureModels db = new SunExposureModels();
        private JMGContext db = new JMGContext();

        // GET: api/SunExposures
        public IQueryable<SunExposure> GetsunExposures()
        {
            return db.sunExposures;
        }

        // GET: api/SunExposures/5
        [ResponseType(typeof(SunExposure))]
        public IHttpActionResult GetSunExposure(int id)
        {
            SunExposure sunExposure = db.sunExposures.Find(id);
            if (sunExposure == null)
            {
                return NotFound();
            }

            return Ok(sunExposure);
        }

        // PUT: api/SunExposures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSunExposure(int id, SunExposure sunExposure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sunExposure.sunExposureID)
            {
                return BadRequest();
            }

            db.Entry(sunExposure).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SunExposureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SunExposures
        [ResponseType(typeof(SunExposure))]
        public IHttpActionResult PostSunExposure(SunExposure sunExposure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.sunExposures.Add(sunExposure);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sunExposure.sunExposureID }, sunExposure);
        }

        // DELETE: api/SunExposures/5
        [ResponseType(typeof(SunExposure))]
        public IHttpActionResult DeleteSunExposure(int id)
        {
            SunExposure sunExposure = db.sunExposures.Find(id);
            if (sunExposure == null)
            {
                return NotFound();
            }

            db.sunExposures.Remove(sunExposure);
            db.SaveChanges();

            return Ok(sunExposure);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SunExposureExists(int id)
        {
            return db.sunExposures.Count(e => e.sunExposureID == id) > 0;
        }
    }
}