﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;

namespace JMG.Controllers
{
    public class BugTypesController : ApiController
    {
        private BugTypeModels db = new BugTypeModels();

        // GET: api/BugTypes
        public IQueryable<BugType> GetBugTypes()
        {
            return db.BugTypes;
        }

        // GET: api/BugTypes/5
        [ResponseType(typeof(BugType))]
        public IHttpActionResult GetBugType(int id)
        {
            BugType bugType = db.BugTypes.Find(id);
            if (bugType == null)
            {
                return NotFound();
            }

            return Ok(bugType);
        }

        // PUT: api/BugTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBugType(int id, BugType bugType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bugType.bugTypeID)
            {
                return BadRequest();
            }

            db.Entry(bugType).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BugTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BugTypes
        [ResponseType(typeof(BugType))]
        public IHttpActionResult PostBugType(BugType bugType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BugTypes.Add(bugType);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = bugType.bugTypeID }, bugType);
        }

        // DELETE: api/BugTypes/5
        [ResponseType(typeof(BugType))]
        public IHttpActionResult DeleteBugType(int id)
        {
            BugType bugType = db.BugTypes.Find(id);
            if (bugType == null)
            {
                return NotFound();
            }

            db.BugTypes.Remove(bugType);
            db.SaveChanges();

            return Ok(bugType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BugTypeExists(int id)
        {
            return db.BugTypes.Count(e => e.bugTypeID == id) > 0;
        }
    }
}