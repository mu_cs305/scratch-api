﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;

namespace JMG.Controllers
{
    public class StudentsController : ApiController
    {
        //private StudentModels db = new StudentModels();

        private JMGContext db = new JMGContext();

        // GET: api/Students
        public IQueryable<Student> GetStudents()
        {
            return db.Students.Where(s => s.isDeleted == false);
        }

        //@TODO: test!!!
        // GET: api/Students/unfiltered
        [Route("api/students/unfiltered")]
        public IQueryable<Student> GetStudentsUnfiltered()
        {
            return db.Students
                .Include(s => s.testSessions);
        }

        // GET: api/Students/5
        [ResponseType(typeof(Student))]
        public IHttpActionResult GetStudent(string id)
        {
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // PUT: api/Students/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStudent(string id, Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student.studentID)
            {
                return BadRequest();
            }           
            db.Entry(student).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Students
        [ResponseType(typeof(Student))]
        public IHttpActionResult PostStudent(Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Students.Add(student);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (StudentExists(student.studentID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = student.studentID }, student);
        }

        // DELETE: api/Students/5
        //[ResponseType(typeof(Student))]
        //public IHttpActionResult DeleteStudent(Student posted)
        //{
        //    Student student = db.Students.Find(posted.studentID);
        //    if (student == null)
        //    {
        //        return NotFound();
        //    }

        //    //db.Students.Remove(student);
        //    //Students should be soft-deleted:
        //    student.isDeleted = true;
        //    db.SaveChanges();

        //    return Ok(student);
        //}

        [ResponseType(typeof(Student))]
        [AcceptVerbs("POST")]
        [Route("api/students/archive")]
        public IHttpActionResult ArchiveStudent(Student posted)
        {
            Student student = db.Students.Find(posted.studentID);
            if (student == null)
            {
                return NotFound();
            }

            //db.Students.Remove(student);
            //Students should be soft-deleted:
            student.isDeleted = true;
            try
            {
                db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

            return Ok(student);
        }


        [ResponseType(typeof(Student))]
        [AcceptVerbs("POST")]
        [Route("api/students/unarchive")]
        public IHttpActionResult UnarchiveStudent(Student posted)
        {
            Student student = db.Students.Find(posted.studentID);
            if (student == null)
            {
                return NotFound();
            }

            //db.Students.Remove(student);
            //Students should be soft-deleted:
            student.isDeleted = false;
            try
            {
                db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

            return Ok(student);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentExists(string id)
        {
            return db.Students.Count(e => e.studentID == id) > 0;
        }
    }
}