﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;

namespace JMG.Controllers
{
    public class TestSessionsController : ApiController
    {
        //private TestSessionModels db = new TestSessionModels();
        private JMGContext db = new JMGContext();

        // GET: api/TestSessions
        public IQueryable<TestSession> GetTestSessions()
        {
            return db.TestSessions;
        }

        // GET: api/TestSessions/5
        [ResponseType(typeof(TestSession))]
        public IHttpActionResult GetTestSession(int id)
        {
            TestSession testSession = db.TestSessions.Find(id);
            if (testSession == null)
            {
                return NotFound();
            }

            return Ok(testSession);
        }

        // PUT: api/TestSessions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTestSession(int id, TestSession testSession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != testSession.testSessionId)
            {
                return BadRequest();
            }

            db.Entry(testSession).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestSessionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TestSessions
        [ResponseType(typeof(TestSession))]
        public IHttpActionResult PostTestSession(TestSessionPostObject data)
        {
            //Attach student object
            //db.Students.Attach(data.student);
            foreach (Answer a in data.session.answers)
            {
                db.Answers.Add(a);
            }
            db.SaveChanges();
            foreach (Answer a in data.session.answers)
            {
                db.Answers.Attach(a);
            }

            TestSession toAdd = new TestSession
            {
                assessmentDate = data.session.assessmentDate,
                startTime = data.session.startTime,
                completionTime = data.session.completionTime,
                answers = data.session.answers,
                assessment = db.Assessments.First(a => a.assessmentID == data.session.assessment.assessmentID)
            };
            TestSession added = db.TestSessions.Add(toAdd);
            
            db.SaveChanges();
            db.Students.First(s => s.studentID == data.student.studentID).testSessions.Add(
                db.TestSessions.First(ts => ts.testSessionId == added.testSessionId));
            db.SaveChanges();

            return Ok(toAdd);
        }

        // DELETE: api/TestSessions/5
        [ResponseType(typeof(TestSession))]
        public IHttpActionResult DeleteTestSession(int id)
        {
            TestSession testSession = db.TestSessions.Find(id);
            if (testSession == null)
            {
                return NotFound();
            }

            db.TestSessions.Remove(testSession);
            db.SaveChanges();

            return Ok(testSession);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TestSessionExists(int id)
        {
            return db.TestSessions.Count(e => e.testSessionId == id) > 0;
        }
    }
}