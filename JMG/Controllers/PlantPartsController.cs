﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;

namespace JMG.Controllers
{
    public class PlantPartsController : ApiController
    {
        //private PlantPartModels db = new PlantPartModels();
        private JMGContext db = new JMGContext();

        // GET: api/PlantParts
        public IQueryable<PlantPart> GetPlantParts()
        {
            return db.PlantParts;
        }

        // GET: api/PlantParts/5
        [ResponseType(typeof(PlantPart))]
        public IHttpActionResult GetPlantPart(int id)
        {
            PlantPart plantPart = db.PlantParts.Find(id);
            if (plantPart == null)
            {
                return NotFound();
            }

            return Ok(plantPart);
        }

        // PUT: api/PlantParts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPlantPart(int id, PlantPart plantPart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != plantPart.plantPartID)
            {
                return BadRequest();
            }

            db.Entry(plantPart).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlantPartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PlantParts
        [ResponseType(typeof(PlantPart))]
        public IHttpActionResult PostPlantPart(PlantPart plantPart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PlantParts.Add(plantPart);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = plantPart.plantPartID }, plantPart);
        }

        // DELETE: api/PlantParts/5
        [ResponseType(typeof(PlantPart))]
        public IHttpActionResult DeletePlantPart(int id)
        {
            PlantPart plantPart = db.PlantParts.Find(id);
            if (plantPart == null)
            {
                return NotFound();
            }

            db.PlantParts.Remove(plantPart);
            db.SaveChanges();

            return Ok(plantPart);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlantPartExists(int id)
        {
            return db.PlantParts.Count(e => e.plantPartID == id) > 0;
        }
    }
}