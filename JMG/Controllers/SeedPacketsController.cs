﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.Models;
using JMG.DAL;

namespace JMG.Controllers
{
    public class SeedPacketsController : ApiController
    {
        //private SeedPacketModels db = new SeedPacketModels();
        private JMGContext db = new JMGContext();

        // GET: api/SeedPackets
        public IQueryable<SeedPacket> GetSeedPackets()
        {
            return db.SeedPackets;
        }

        // GET: api/SeedPackets/5
        [ResponseType(typeof(SeedPacket))]
        public IHttpActionResult GetSeedPacket(int id)
        {
            SeedPacket seedPacket = db.SeedPackets.Find(id);
            if (seedPacket == null)
            {
                return NotFound();
            }

            return Ok(seedPacket);
        }

        // PUT: api/SeedPackets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSeedPacket(int id, SeedPacket seedPacket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != seedPacket.seedPacketID)
            {
                return BadRequest();
            }

            db.Entry(seedPacket).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SeedPacketExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SeedPackets
        [ResponseType(typeof(SeedPacket))]
        public IHttpActionResult PostSeedPacket(SeedPacket seedPacket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SeedPackets.Add(seedPacket);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = seedPacket.seedPacketID }, seedPacket);
        }

        // DELETE: api/SeedPackets/5
        [ResponseType(typeof(SeedPacket))]
        public IHttpActionResult DeleteSeedPacket(int id)
        {
            SeedPacket seedPacket = db.SeedPackets.Find(id);
            if (seedPacket == null)
            {
                return NotFound();
            }

            db.SeedPackets.Remove(seedPacket);
            db.SaveChanges();

            return Ok(seedPacket);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SeedPacketExists(int id)
        {
            return db.SeedPackets.Count(e => e.seedPacketID == id) > 0;
        }
    }
}