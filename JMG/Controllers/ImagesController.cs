﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JMG.DAL;
using JMG.Models;

namespace JMG.Controllers
{
    public class ImagesController : ApiController
    {
        private JMGContext db = new JMGContext();

        // GET: api/Images
        public IQueryable<Image> GetImages()
        {
            return db.Images;
        }

        // GET: api/Images/5
        [ResponseType(typeof(Image))]
        public IHttpActionResult GetImage(int id)
        {
            Image image = db.Images.Find(id);
            if (image == null)
            {
                return NotFound();
            }

            return Ok(image);
        }

        // PUT: api/Images/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImage(int id, Image image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != image.imageID)
            {
                return BadRequest();
            }

            db.Entry(image).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //PUT: api/Images/bug/1/5
        [ResponseType(typeof(void))]
        [Route("api/Images/bug/{bugID}/{imageID}")]
        public IHttpActionResult PutBugSprite(int bugID, int imageID, BugSprite image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (imageID != image.imageID)
            {
                return BadRequest();
            }            

            db.Entry(image).State = EntityState.Modified;
            db.SaveChanges();

            //If this image is being used for bug match, disable other images from being used for bug match
            if (image.bugMatchInd)
            {
                Bug bug = db.Bugs.Find(bugID);
                foreach (BugSprite other in bug.Images)
                {
                    if (other.imageID != image.imageID)
                    {
                        BugSprite othr = (BugSprite)db.Images.Find(other.imageID);
                        othr.bugMatchInd = false;
                    }
                }
            }
            //If this image is being used for bug sort, disable other images from being used for bug sort
            if (image.bugSortInd)
            {
                Bug bug = db.Bugs.Find(bugID);
                foreach (BugSprite other in bug.Images)
                {
                    if (other.imageID != image.imageID)
                    {
                        BugSprite othr = (BugSprite)db.Images.Find(other.imageID);
                        other.bugSortInd = false;
                    }
                }
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageExists(imageID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //PUT: api/Images/plant/1/5
        [ResponseType(typeof(void))]
        [Route("api/Images/plant/{plantID}/{imageID}")]
        public IHttpActionResult PutPlantSprite(int plantID, int imageID, PlantSprite image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (imageID != image.imageID)
            {
                return BadRequest();
            }
            
            db.Entry(image).State = EntityState.Modified;

            db.SaveChanges();

            //If this image is being used for plant match, disable other images from being used for plant match
            if (image.plantMatchInd)
            {
                Plant plant = db.Plants.Find(plantID);
                foreach (PlantSprite other in plant.Images)
                {
                    if (other.imageID != image.imageID)
                    {
                        other.plantMatchInd = false;
                        db.Entry(other).State = EntityState.Modified;
                    }
                }
            }
            //If this image is being used for fruit-veggie, disable other images from being used for fruit-veggie
            if (image.fruitVeggieInd)
            {
                Plant plant = db.Plants.Find(plantID);
                foreach (PlantSprite other in plant.Images)
                {
                    if (other.imageID != image.imageID)
                    {
                        other.fruitVeggieInd = false;
                        db.Entry(other).State = EntityState.Modified;
                    }
                }
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageExists(imageID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Images/bug/5
        [ResponseType(typeof(BugSprite))]
        [Route("api/Images/bug/{bugID}")]
        public IHttpActionResult PostBugSprite(BugSprite image, int bugID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Bug bug = db.Bugs.Find(bugID);
            bug.Images.Add(image);

            db.SaveChanges();

            return Ok(image);
        }

        // POST: api/Images/plant/5
        [ResponseType(typeof(PlantSprite))]
        [Route("api/Images/plant/{plantID}")]
        public IHttpActionResult PostPlantSprite(PlantSprite image, int plantID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Plant plant = db.Plants.Find(plantID);
            plant.Images.Add(image);

            db.SaveChanges();

            return Ok(image);
        }

        // DELETE: api/Images/5
        [ResponseType(typeof(Image))]
        public IHttpActionResult DeleteImage(int id)
        {
            Image image = db.Images.Find(id);
            if (image == null)
            {
                return NotFound();
            }

            db.Images.Remove(image);
            db.SaveChanges();

            return Ok(image);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ImageExists(int id)
        {
            return db.Images.Count(e => e.imageID == id) > 0;
        }
    }
}