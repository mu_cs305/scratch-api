﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using JMG.Providers;
using Microsoft.AspNet.Identity;
using JMG.DAL;
using Microsoft.AspNet.Identity.EntityFramework;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(JMG.Startup))]

namespace JMG
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
            
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
          
           
             config.EnableSwagger(c => c.SingleApiVersion("v1", "Scratch API"))
             .EnableSwaggerUi();
            

            //create superuser account:
            AuthContext context = new AuthContext();
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            userManager.Create(new ApplicationUser
            {
                UserName = "superuser@super.com",
                FirstName = "Developer",
                LastName = "Account",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                isDeleted = false,
                isAdmin = true,
            }, "SuperPass");

            userManager.Create(new ApplicationUser
            {
                UserName = "muellerleile@marshall.edu",
                FirstName = "Paige",
                LastName = "Muellerleile",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                isDeleted = false,
                isAdmin = true,
            }, "ScratchMaster");

        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/gettoken"),
                //@TODO: 24 hours is a really long token expiry.  What should we set this to?  3 hours maybe?
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
    }
}
