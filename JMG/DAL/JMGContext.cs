﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JMG.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Data.Entity.Migrations;

namespace JMG.DAL
{
    public class JMGContext : DbContext
    {

        // This class is used by the Seed() method in Migrations/Configuration.cs 
        
        public JMGContext() : base("JMGConnection")
        {            
        }

        public DbSet<SeedPacket> SeedPackets { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Assessment> Assessments { get; set; }
        public DbSet<Bug> Bugs { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<PlantPart> PlantParts {get ; set;}
        public DbSet<TestSession> TestSessions { get; set; }
        public DbSet<Soil> Soils { get; set; }
        public DbSet<SunExposure> sunExposures { get; set; }
        public DbSet<PlantPartList> PlantPartLists { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<EdiblePart> EdibleParts { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<GameSettings> GameSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           // Database.SetInitializer<JMGContext>(new JMGInitializer());
          
            JMGContext db = new JMGContext();
            db.Database.CreateIfNotExists();
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();

            // Defines a 1:N between plant and seed packet. 
         //    modelBuilder.Entity<SeedPacket>().HasRequired(f => f.plant);

        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

    }


        }


