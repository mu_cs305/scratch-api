﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using JMG.Models;

namespace JMG.DAL
{
//    public class JMGInitializer : DropCreateDatabaseIfModelChanges<JMGContext>
    public class JMGInitializer : CreateDatabaseIfNotExists<JMGContext>
    {
        protected override void Seed(JMGContext context)
        {
            
            base.Seed(context);

            var plantParts = new List<PlantPart>
            {
                new PlantPart {plantPartName = "Seed" },
                new PlantPart {plantPartName = "Fruit" },
                new PlantPart {plantPartName = "Leaves" },
                new PlantPart {plantPartName = "Roots" },
                new PlantPart {plantPartName = "Stem" },
                new PlantPart {plantPartName = "Flowers" },
            };
                plantParts.ForEach(plant => context.PlantParts.Add(plant));
                context.SaveChanges();


                var ediblePartsList = new List<EdiblePart>
            {
                new EdiblePart {plantPartName = "Seed" },
                new EdiblePart {plantPartName = "Fruit" },
                new EdiblePart {plantPartName = "Leaves" },
                new EdiblePart {plantPartName = "Roots" },
                new EdiblePart {plantPartName = "Stem" },
                new EdiblePart {plantPartName = "Flowers" },
            };
                ediblePartsList.ForEach(part => context.EdibleParts.Add(part));
                context.SaveChanges();

            /* Plant list 
                Beets, Broccoli, Carrots, Cauliflower, 
                Collared Greens, Corn, Cucumber, Eggplant,
                Green Beans, Lettuce, Jalapeno Pepper,
                Okra, Onion, Peas, Pepper, Potato, Pumpkin,
                Radish, Tomato, Squash, Watermelon
            */



            Plant beets = new Plant
            {
                plantName = "Beets",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(beets);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Beets").Images = new List<Image>
            {
                new PlantPicture
                {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Beets"),
                    imageName = "Beet pic",
                    imageDescription = "Just some beets",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116900/beetroot-904371_1280_mp0pnu.jpg",
                    imageWidth = 1280,
                    imageHeight = 857,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant bberry = new Plant
            {
                plantName = "Blueberries",
                isColdSeasonPlant = false,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(bberry);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Blueberries").Images = new List<Image>
            {
                new PlantPicture
                {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Blueberries"),
                    imageName = "Blueberries",
                    imageDescription = "Just some blueberries",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116899/blueberry-873784_1280_azgxrd.jpg",
                    imageWidth = 1280,
                    imageHeight = 895,
                    isDefault = true,
                },

            };

            context.SaveChanges();
            Plant broc = new Plant
            {
                plantName = "Broccoli",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,

            };
            context.Plants.Add(broc);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Broccoli").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Broccoli"),
                    imageName = "Broccoli",
                    imageDescription = "Cabbage with a crown",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116900/broccoli-390002_1280_dlyx9y.jpg",
                    imageWidth = 1280,
                    imageHeight = 866,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant crt = new Plant
            {
                plantName = "Carrots",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,

            };
            context.Plants.Add(crt);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Carrots").Images = new List<Image>
            {
                new PlantPicture
                {
                     PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Carrots"),
                    imageName = "Carrot",
                    imageDescription = "They weren't always orange",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116902/carrots-874981_1280_cvl8ka.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant caul = new Plant
            {
                plantName = "Cauliflower",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(caul);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Cauliflower").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Cauliflower"),
                    imageName = "Cauliflower",
                    imageDescription = "Something about cauliflower",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116904/cauliflower_nu2xta.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant collardGreens = new Plant
            {
                plantName = "Collared Greens",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(collardGreens);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Collared Greens").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Collared Greens"),
                    imageName = "Collared Greens",
                    imageDescription = "By the collar",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116919/Collard-Greens-Bundle_cjh19g.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = false,
                },
            };
            context.SaveChanges();

            Plant corn = new Plant
            {
                plantName = "Corn",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(corn);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Corn").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Corn"),
                    imageName = "Corn",
                    imageDescription = "Something something corn",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116902/corn-917354_1280_colage.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();



            Plant cucumber = new Plant
            {
                plantName = "Cucumber",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(cucumber);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Cucumber").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Cucumber"),
                    imageName = "Cucumber",
                    imageDescription = "Something something cucumber",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116906/cucumbers-863808_1280_c4pxyf.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();


            Plant eggPlant = new Plant
            {
                plantName = "Eggplant",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(eggPlant);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Eggplant").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Eggplant"),
                    imageName = "Eggplant",
                    imageDescription = "Something something eggplant",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116904/eggplant-839859_1280_djt2tj.jpg",
                     imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();


            Plant greenBeans = new Plant
            {
                plantName = "Green Bean",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(greenBeans);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Green Bean").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Green Bean"),
                    imageName = "Green Bean",
                    imageDescription = "Something something green bean",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116903/green-beans-519439_1280_jnk0am.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant lettuce = new Plant
            {
                plantName = "Lettuce",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = false,
            };
            context.Plants.Add(lettuce);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Lettuce").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Lettuce"),
                    imageName = "Lettuce",
                    imageDescription = "Something something lettuce",
                    fileType = ".jpg",
                    imageUri ="http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116908/lettuce-940032_1280_mhv8dx.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();


            Plant jalapeno = new Plant
            {
                plantName = "Jalapeno Pepper",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(jalapeno);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Jalapeno Pepper").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Jalapeno Pepper"),
                    imageName = "Jalapeno Pepper",
                    imageDescription = "Something something jalapeno pepper",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116908/jalapeno-282054_1280_dpif8q.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant okra = new Plant
            {
                plantName = "Okra",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(okra);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Okra").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Okra"),
                    imageName = "Okra",
                    imageDescription = "Something something okra",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116908/okra-552750_1280_fyqs54.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant onion = new Plant
            {
                plantName = "Onion",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(onion);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Onion").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Onion"),
                    imageName = "Onion",
                    imageDescription = "Something something onion",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116909/onion836837_1280_u9cpup.jpg",
                     imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant peas = new Plant
            {
                plantName = "Peas",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(peas);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Peas").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Peas"),
                    imageName = "Peas",
                    imageDescription = "Something something peas",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116908/pod-855267_1280_plwl65.png",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();


            Plant pepper = new Plant
            {
                plantName = "Pepper",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(pepper);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Pepper").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Pepper"),
                    imageName = "Pepper",
                    imageDescription = "Something something pepper",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116912/peppers-154377_1280_a9jswl.png",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant potato = new Plant
            {
                plantName = "Potato",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(potato);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Potato").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Potato"),
                    imageName = "Potato",
                    imageDescription = "Something something potato",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116910/potato-2795_1280_kb4qv7.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant pumpkin = new Plant
            {
                plantName = "Pumpkin",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(pumpkin);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Pumpkin").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Pumpkin"),
                    imageName = "Pumpkin",
                    imageDescription = "Something something pumpkin",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116912/pumpkins-432607_1280_roplsg.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant radish = new Plant
            {
                plantName = "Radish",
                isColdSeasonPlant = true,
                isHotSeasonPlant = true,
                isFruit = false,
            };
            context.Plants.Add(radish);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Radish").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Radish"),
                    imageName = "Radish",
                    imageDescription = "Something something radish",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448118129/radish-673200_1280_lhfsdo.jpg",
                     imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant strawBerry = new Plant
            {
                plantName = "Strawberry",
                isColdSeasonPlant = true,
                isHotSeasonPlant = false,
                isFruit = true,
            };
            context.Plants.Add(strawBerry);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Strawberry").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Strawberry"),
                    imageName = "Strawberry",
                    imageDescription = "Something something strawberry",
                    fileType = ".jpg",
                    imageUri ="http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116912/strawberry-272810_1280_m5tfd7.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant tomato = new Plant
            {
                plantName = "Tomato",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(tomato);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Tomato").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Tomato"),
                    imageName = "Tomato",
                    imageDescription = "Something something tomato",
                    fileType = ".jpg",
                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448118202/tomato-899090_1280_hwlr7x.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant squash = new Plant
            {
                plantName = "Squash",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(squash);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Squash").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Squash"),
                    imageName = "Squash",
                    imageDescription = "Something something squash",
                    fileType = ".jpg",
                    imageUri ="http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116899/butternut-72295_1280_vzx5ut.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();

            Plant watermelon = new Plant
            {
                plantName = "Watermelon",
                isColdSeasonPlant = false,
                isHotSeasonPlant = true,
                isFruit = true,
            };
            context.Plants.Add(watermelon);
            context.SaveChanges();
            context.Plants.FirstOrDefault(s => s.plantName == "Watermelon").Images = new List<Image>
            {
                new PlantPicture {
                    PlantModel = context.Plants.FirstOrDefault(s => s.plantName == "Watermelon"),
                    imageName = "Watermelon",
                    imageDescription = "Something something watermelon",
                    fileType = ".jpg",
                    imageUri ="http://res.cloudinary.com/duyfjnu2h/image/upload/v1448116913/watermelon-630276_1280_g3mnpu.jpg",
                    imageWidth = 1280,
                    imageHeight = 853,
                    isDefault = true,
                },
            };
            context.SaveChanges();






            Bug ant = new Bug
            {
                bugName = "Ant",
                BugType = BugType.Pest,
            };

            context.Bugs.Add(ant);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Ant").Images = new List<Image> {
                        new BugPicture
                        {
                            BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Ant"),
                            imageName = "Ant",
                            imageDescription = "This insect sometimes goes marching",
                            fileType = ".jpg",
                            imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448119474/ant-564617_1280_if3cqc.jpg",
                            imageWidth = 600,
                            imageHeight = 800,
                        },
                    };
            context.SaveChanges();

            Bug aphid = new Bug
            {
                bugName = "Aphid",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(aphid);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Aphid").Images = new List<Image> {
                                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Aphid"),
                                    imageName = "Aphid",
                                    imageDescription = "Nothing funny about aphids",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048627/aphid-563256_1280_gibhvm.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
                            };
            context.SaveChanges();

            Bug bee = new Bug
            {
                bugName = "Bee",
                BugType = BugType.Pollinator,
            };
            context.Bugs.Add(bee);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Bee").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Bee"),

                                    imageName = "Bee",
                                    imageDescription = "Always busy",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048626/bee-814325_1280_gdjw6m.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug butterfly = new Bug
            {
                bugName = "Butterfly",
                BugType = BugType.Pollinator,
                //  assetUrl = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048627/butterfly-491166_1280_cfu6tm.jpg"
            };
            context.Bugs.Add(butterfly);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Butterfly").Images = new List<Image>
            {

                                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Butterfly"),
                                    imageName = "Butterfly",
                                    imageDescription = "Colorful wings ",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048627/butterfly-491166_1280_cfu6tm.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },

            };
            context.SaveChanges();

            Bug earthworm = new Bug
            {
                bugName = "Earthworm",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(earthworm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Earthworm").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Earthworm"),
                                    imageName = "Earthworm",
                                    imageDescription = "Nature's squirmy soil aerator",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048628/earthworm-694510_1280_iof7he.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug earwig = new Bug
            {
                bugName = "Earwig",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(earwig);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Earwig").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Earwig"),
                                    imageName = "Earwig",
                                    imageDescription = "Not a hairpiece for your ears.",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/earwig-560780_1280_qgaj1k.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug fly = new Bug
            {
                bugName = "Fly",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(fly);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Fly").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Fly"),
                                    imageName = "Fly",
                                    imageDescription = "Makes a buzz nobody loves to hear.",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048627/fly-946712_1280_mcx1n1.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug gBeetle = new Bug
            {
                bugName = "Ground Beetle",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(gBeetle);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Ground Beetle").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Ground Beetle"),
                                    imageName = "Ground Beetle",
                                    imageDescription = "This bug keeps its nose to the ground.",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448119698/ground_beetle-2821B_jt4hnk.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug jBeetle = new Bug
            {
                bugName = "Japanese Beetle",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(jBeetle);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Japanese Beetle").Images = new List<Image>
            {
                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Japanese Beetle"),
                                    imageName = "Japanese Beetle",
                                    imageDescription = "Big in Japan and in America.",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048636/japanese_beetle_fbeurf.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug lWing = new Bug
            {
                bugName = "Lacewing",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(lWing);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Lacewing").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Lacewing"),
                                    imageName = "Lacewing",
                                    imageDescription = "Pithy description here",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048630/lacewing-455965_1280_nz5qfp.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug lbug = new Bug()
            {
                bugName = "Ladybug",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(lbug);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Ladybug").Images = new List<Image>
            {
                   new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Ladybug"),
                                    imageName = "Ladybug",
                                    imageDescription = "This predator's not always a lady",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048630/ladybug-241636_1280_rly88g.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();


            Bug mBug = new Bug
            {
                bugName = "Mealy bug",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(mBug);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Mealy bug").Images = new List<Image>
            {
                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Mealy bug"),
                                    imageName = "Mealy bug",
                                    imageDescription = "Everybody's looking for a meal",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048637/Mealybugs_of_flower_stem__Yogyakarta__2014-10-31_ykopgj.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug midge = new Bug
            {
                bugName = "Midge",
                BugType = BugType.Pollinator,
            };
            context.Bugs.Add(midge);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Midge").Images = new List<Image>
            {
                   new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Midge"),
                                    imageName = "Midge",
                                    imageDescription = "Something pithy here",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048628/fly-640860_1280_muuoqj.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug moth = new Bug
            {
                bugName = "Moth",
                BugType = BugType.Pollinator,
            };
            context.Bugs.Add(moth);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Moth").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Moth"),
                                    imageName = "Moth",
                                    imageDescription = "Goes towards the light",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048629/moth-238937_1280_bkapb4.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();


            Bug nem = new Bug
            {
                bugName = "Nematode",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(nem);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Nematode").Images = new List<Image>
            {
                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Nematode"),
                                    imageName = "Nematode",
                                    imageDescription = "Nothing to do with amphibians",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048628/nematode_ikhkcr.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug pm = new Bug
            {
                bugName = "Praying Mantis",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(pm);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Praying Mantis").Images = new List<Image>
            {
                   new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Praying Mantis"),
                                    imageName = "Praying mantis",
                                    imageDescription = "A righteous predator",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048635/praying-mantis-205165_1280_i3jjsk.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug slug = new Bug
            {
                bugName = "Slug",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(slug);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Slug").Images = new List<Image>
            {
                    new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Slug"),
                                    imageName = "Slug",
                                    imageDescription = "Leaves a shiny trail",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048635/tigerschnecke-332165_1280_ba5era.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },

            };
            context.SaveChanges();

            Bug snail = new Bug
            {
                bugName = "Snail",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(snail);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Snail").Images = new List<Image>
            {
                       new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Snail"),
                                    imageName = "Snail",
                                    imageDescription = "The pace-setter",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/snails-382992_1280_a0bfeo.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();


            Bug spider = new Bug
            {
                bugName = "Spider",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(spider);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Spider").Images = new List<Image>
            {
                                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Spider"),
                                    imageName = "Spider",
                                    imageDescription = "Our pal the arachnid",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048631/spider-564685_1280_mlyadv.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },

            };
            context.SaveChanges();

            Bug sMite = new Bug
            {
                bugName = "Spider mite",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(sMite);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Spider mite").Images = new List<Image>
            {
                    new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Spider mite"),
                                    imageName = "Spider mite",
                                    imageDescription = "Not your average spider",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/springtail_giqqyq.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug stail = new Bug
            {
                bugName = "Springtail",
                BugType = BugType.Pooper,
            };
            context.Bugs.Add(stail);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Springtail").Images = new List<Image>
            {
                new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Springtail"),
                                    imageName = "Springtail",
                                    imageDescription = "Found in summer, too",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048632/springtail_giqqyq.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug svb = new Bug
            {
                bugName = "Squash Vine Borer",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(svb);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Squash Vine Borer").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Squash Vine Borer"),
                                    imageName = "Squah vine borer",
                                    imageDescription = "A boring bug",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048637/squash_vine_borer_fttqnj.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug sb = new Bug
            {
                bugName = "Stink Bug",
                BugType = BugType.Pest,
            };
            context.Bugs.Add(sb);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Stink Bug").Images = new List<Image>
            {
                 new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Stink Bug"),
                                    imageName = "Stink Bug",
                                    imageDescription = "Kind of a stinker",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048633/stink_bug_pyzx1v.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
            };
            context.SaveChanges();

            Bug wasp = new Bug
            {
                bugName = "Wasp",
                BugType = BugType.Predator,
            };
            context.Bugs.Add(wasp);
            context.SaveChanges();
            context.Bugs.FirstOrDefault(s => s.bugName == "Wasp").Images = new List<Image>
            {
                   new BugPicture
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Wasp"),
                                    imageName = "European wasp",
                                    imageDescription = "A real stringer",
                                    fileType = ".jpg",
                                    imageUri = "http://res.cloudinary.com/duyfjnu2h/image/upload/v1448048634/wasp-814323_1280_bdi9dj.jpg",
                                    imageWidth = 600,
                                    imageHeight = 800,
                                },
                                new BugSprite
                                {
                                    BugModel = context.Bugs.FirstOrDefault(s=>s.bugName=="Wasp"),
                                    imageName = "European wasp",
                                    imageDescription = "Sprite for a euro wasp",
                                    fileType = ".gif",

                                    // Don't use this one in production! 
                                    imageUri = "http://heavycatstudios.com/resource/1002-0017.gif",
                                    imageWidth = 250,
                                    imageHeight = 250,

                                },
            };
            context.SaveChanges();


     

            
            /* Plant list 
                Beets, Broccoli, Carrots, Cauliflower, 
                Collared Greens, Corn, Cucumber, Eggplant,
                Green Beans, Lettuce, Jalapeno Pepper,
                Okra, Onion, Peas, Pepper, Potato, Pumpkin,
                Radish, Tomato, Squash, Watermelon
            */


            context.SeedPackets.Add(new SeedPacket
                {
                    nickname = "Burpee Beet Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Beets"),
                });

                context.SeedPackets.Add(new SeedPacket
                {
                    nickname = "Burpee Broccoli Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Broccoli"),
                });

                context.SeedPackets.Add(new SeedPacket
                {
                    nickname = "Burpee Carrot Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Carrots"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Cauliflower Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Cauliflower"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "American Seed Collared Green Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Collared Greens"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Corn Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Corn"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Cucumber Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Cucumber"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Hart's Eggplant Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Eggplant"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Burpee Organic Green Bean Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Green Beans"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Flashy Butter Oak Lettuce Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Lettuce"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Hart's Jalapeno Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Jalapeno Pepper"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Okra Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Okra"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "High Mowing Organic Onion Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Onion"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Dwarf Grey Sugar Pea Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Peas"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Hart's California Wonder Pepper Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Pepper"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Seed Potatoes",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Potato"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Winter Luxury Pie Pumpkin Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Pumpkin"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Radish Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Radish"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Baker Creek Mortgage Lifter Tomato Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Tomato"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Hart's Squash Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Squash"),
                });

                context.SeedPackets.Add( new SeedPacket
                {
                    nickname = "Burpee Watermelon Seeds",
                    plant = context.Plants.FirstOrDefault(s => s.plantName == "Watermelon"),
                });

                context.SaveChanges();


            //  Seed an assessment:

            List<Bug> bugList = new List<Bug>();
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 1));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 2));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 3));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 4));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 5));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 6));
            bugList.Add(context.Bugs.FirstOrDefault(b => b.BugID == 7));

            List<SeedPacket> packetList = new List<SeedPacket>();
            packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 1));
            packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 3));
            packetList.Add(context.SeedPackets.FirstOrDefault(p => p.seedPacketID == 5));
            Assessment seedAssessment = new Assessment
            {
                description = "The default assessment.  Contains seven bugs and three seed packets.",
                dateCreated = DateTime.Now,
                isDeleted = false,
                bugs = bugList,
                seedPackets = packetList,
            };
            Assessment added = context.Assessments.Add(seedAssessment);
            context.SaveChanges();


            //Seed a couple test sessions for testing purposes
            //@TODO: remove before before production
            
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Ant",
                correctChoice = "Ant",
                choices = "Ant, Bee, Earthworm",
                chapter = 4
            });
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Aphid",
                correctChoice = "Aphid",
                choices = "Aphid, Butterfly, Centipede",
                chapter = 4
            });
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Bee",
                correctChoice = "Cabbage Worm",
                choices = "Bee, Butterfly, Cabbage Worm",
                chapter = 4
            });
            context.SaveChanges();

            List<Answer> answersList = new List<Answer>();
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Bee"));
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Aphid"));
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Ant"));

            TestSession seedTS = new TestSession
            {
                assessmentDate = new DateTime(2013, 12, 14),
                startTime = new DateTime(2013, 12, 14),
                completionTime = new DateTime(2013, 12, 14).AddMinutes(10.5),
                assessment = context.Assessments.FirstOrDefault(a => a.assessmentID == added.assessmentID),
                answers = answersList
            };

            context.TestSessions.Add(seedTS);
            context.SaveChanges();

            //TS2 for testing reports
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Wasp",
                correctChoice = "Ant",
                choices = "Ant, Wasp, Earthworm",
                chapter = 4
            });
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Butterfly",
                correctChoice = "Aphid",
                choices = "Aphid, Butterfly, Centipede",
                chapter = 4
            });
            context.Answers.Add(new Answer
            {
                question = "Zap the bug who is a pest!",
                studentChoice = "Cabbage Worm",
                correctChoice = "Cabbage Worm",
                choices = "Bee, Butterfly, Cabbage Worm",
                chapter = 4
            });
            context.SaveChanges();

            answersList = new List<Answer>();
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Wasp"));
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Butterfly"));
            answersList.Add(context.Answers.FirstOrDefault(a => a.studentChoice == "Cabbage Worm"));

            TestSession seedTS2 = new TestSession
            {
                assessmentDate = new DateTime(2013, 12, 13),
                startTime = new DateTime(2013, 12, 13),
                completionTime = new DateTime(2013, 12, 13).AddMinutes(7),
                assessment = context.Assessments.FirstOrDefault(a => a.assessmentID == added.assessmentID),
                answers = answersList
            };

            context.TestSessions.Add(seedTS2);
            context.SaveChanges();

            Student stud01 = new Student
            {
                studentID = "stud01",
                testSessions = new List<TestSession> {
                    seedTS
                },
                registrationDate = new DateTime(2013, 12, 14),
                isDeleted = false
            };
            context.Students.Add(stud01);
            context.SaveChanges();

            Student stud02 = new Student
            {
                studentID = "stud02",
                testSessions = new List<TestSession> {
                    seedTS2
                },
                registrationDate = new DateTime(2013, 12, 12),
                isDeleted = false
            };
            context.Students.Add(stud02);
            context.SaveChanges();
        }



    }
}