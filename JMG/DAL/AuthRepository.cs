﻿using JMG.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JMG.DAL
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private UserManager<ApplicationUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));
           
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                isDeleted = false,
                isAdmin = userModel.isAdmin,
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            var emailService = new EmailService();
            var message = new IdentityMessage();
            emailService.SendAsync(message);

            return result;
        }

        public IQueryable<ApplicationUser> getAllUsers()
        {
            return _ctx.Users
                .Include(p => p.Claims)
                .Include(p => p.Logins)
                .Include(p => p.Roles)
                .Where(u => u.isDeleted == false);
        }

        public IQueryable<ApplicationUser> getAdmins()
        {
            return _ctx.Users
                .Include(p => p.Claims)
                .Include(p => p.Logins)
                .Include(p => p.Roles)
                .Where(u => u.isAdmin == true);
        }

        public IQueryable<ApplicationUser> getInstructors()
        {
            return _ctx.Users
                .Include(p => p.Claims)
                .Include(p => p.Logins)
                .Include(p => p.Roles)
                .Where(u => u.isAdmin == false);
        }

        public IQueryable<ApplicationUser> getAllUnfiltered()
        {
            return _ctx.Users
                .Include(p => p.Claims)
                .Include(p => p.Logins)
                .Include(p => p.Roles);
        }

        public ApplicationUser GetUserByUsername(string userName)
        {
            return _ctx.Users.FirstOrDefault(p => p.UserName == userName);
        }

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public ApplicationUser UpdateUser(ApplicationUser user)
        {
            ApplicationUser stored = _ctx.Users.FirstOrDefault(u => u.Id == user.Id);
            if (stored != null)
            {
                //This will update everything but password
                stored.UserName = user.UserName;
                stored.AccessFailedCount = user.AccessFailedCount;
                stored.UpdatedDate = DateTime.Now;
                stored.Email = user.UserName;
                stored.EmailConfirmed = user.EmailConfirmed;
                stored.FirstName = user.FirstName;
                stored.LastName = user.LastName;
                stored.isAdmin = user.isAdmin;
                stored.isDeleted = user.isDeleted;
                stored.LockoutEnabled = user.LockoutEnabled;
                stored.LockoutEndDateUtc = user.LockoutEndDateUtc;
                stored.PhoneNumber = user.PhoneNumber;
                stored.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
                stored.SecurityStamp = user.SecurityStamp;
                stored.TwoFactorEnabled = user.TwoFactorEnabled;
                _ctx.SaveChanges();
            }
            return stored;
        }

        //Changes a user's password, old password must be provided for the request to go through
        public ApplicationUser UpdatePassword(string user_id, string oldPassword, string newPassword)
        {
            ApplicationUser stored = _ctx.Users.FirstOrDefault(u => u.Id == user_id);
            if (stored != null)
            {
                PasswordVerificationResult passResult = _userManager.PasswordHasher.VerifyHashedPassword(stored.PasswordHash, oldPassword);
                if(passResult != PasswordVerificationResult.Success)
                {
                    throw new PasswordMismatchException();
                }
                stored.PasswordHash = _userManager.PasswordHasher.HashPassword(newPassword);
                var updateResult = _userManager.Update(stored);
                if (!updateResult.Succeeded)
                {
                    throw new Exception("Failed to update the stored IdentityUser");
                }
            }
            else
            {
                throw new UserNotFoundException();
            }
            return stored;
        }

        

        internal ApplicationUser GetUserById(string id)
        {
            return _ctx.Users.FirstOrDefault(u => u.Id == id);
        }

        internal ApplicationUser ArchiveUser(string id)
        {
            ApplicationUser user = _ctx.Users.FirstOrDefault(u => u.Id == id);
            user.isDeleted = true;
            _ctx.SaveChanges();
            return user;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }

    public class UserNotFoundException : Exception
    {
        public UserNotFoundException()
        {

        }
    }

    public class PasswordMismatchException : Exception
    {
        public PasswordMismatchException()
        {

        }
    }
}