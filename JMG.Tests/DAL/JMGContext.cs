﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using JMG.Models;

namespace JMG.DAL
{
    public class JMGContext : DbContext
    {
        // This class is used by the Seed() method in Migrations/Configuration.cs 
        
        public JMGContext() : base("JMGConnection")
        {

        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Assessment> Assessments { get; set; }
        public DbSet<Bug> Bugs { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<PlantPart> PlantParts {get ; set;}
        public DbSet<SeedPacket> SeedPackets { get;  set;}
        public DbSet<TestSession> TestSessions { get; set; }
        //public DbSet<BugList> BugLists { get; set; }
        //public DbSet<PlantList> PlantLists { get; set; }
        public DbSet<Soil> Soils { get; set; }
   
       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
        }



        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

    }


}



/*


    
namespace MUGADA.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {

        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        //Declaring which classes to create as an Entity in the database

        public virtual DbSet<GradeAppeals> GradeAppeals { get; set; }
        public virtual DbSet<DishonestyAppeals> DishonestyAppeals { get; set; }
        public virtual DbSet<GradeAppealResponses> GradeAppealResponses { get; set; }
        public virtual DbSet<DishonestAppealResponses> DishonestAppealResponses { get; set; }
        public virtual DbSet<GradeType> GradeType { get; set; }
        public virtual DbSet<GradeAppealFiles> GradeAppealFiles { get; set; }
        public virtual DbSet<DishonestyAppealFiles> DishonestyAppealFiles { get; set; }
        public virtual DbSet<ExcludeDates> ExcludeDates { get; set; }
        public virtual DbSet<BAPCMembersInGradeAppeal> BAPCMembersInGradeAppeal { get; set; }
        public virtual DbSet<BAPCMembersInDishonestyAppeal> BAPCMembersInDishonestyAppeal { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GradeAppeals>()
                .Property(e => e.CourseSubject)
                .IsFixedLength();

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ChairGradeAppeals)
                .WithOptional(e => e.Chair)
                .HasForeignKey(e => e.ChairID);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ProfessorGradeAppeals)
                .WithRequired(e => e.Professor)
                .HasForeignKey(e => e.ProfessorID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ProvostGradeAppeals)
                .WithOptional(e => e.Provost)
                .HasForeignKey(e => e.ProvostID);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.StudentGradeAppeals)
                .WithRequired(e => e.Student)
                .HasForeignKey(e => e.StudentID)
                .WillCascadeOnDelete(false);



            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ChairDishonestyAppeals)
                .WithOptional(e => e.Chair)
                .HasForeignKey(e => e.ChairID);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ProfessorDishonestyAppeals)
                .WithRequired(e => e.Professor)
                .HasForeignKey(e => e.ProfessorID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ProvostDishonestyAppeals)
                .WithOptional(e => e.Provost)
                .HasForeignKey(e => e.ProvostID);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.StudentDishonestyAppeals)
                .WithRequired(e => e.Student)
                .HasForeignKey(e => e.StudentID)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<GradeType>()
                .HasMany(e => e.GradeAppeal)
                .WithRequired(e => e.GradeType)
                .WillCascadeOnDelete(false);
        }

*/